<?php
defined('TYPO3_MODE') || die();

call_user_func(function () {

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['hivepoi_hive_poi'] = 'tx_hivepoi_hive_poi';
$tempColumns = [
    'tx_hivepoi_poicustomcssclasses' => [
        'config' => [
            'eval' => 'trim,alphanum_x',
            'placeholder' => 'customclass1 customclass2',
            'type' => 'input',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'Custom CSS classes for the Plugin Wrapper
(seperate with space no starting .)',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivepoi_poicustomcssclasses',
    ],
    'tx_hivepoi_poielement' => [
        'config' => [
            'appearance' => [
                'enabledControls' => [
                    'dragdrop' => '1',
                ],
                'levelLinksPosition' => 'both',
                'useSortable' => '1',
            ],
            'foreign_field' => 'parentid',
            'foreign_sortby' => 'sorting',
            'foreign_table' => 'tx_hivepoi_poielement',
            'foreign_table_field' => 'parenttable',
            'type' => 'inline',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivepoi_poielement',
    ],
    'tx_hivepoi_poigraphic' => [
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => 'tx_hivepoi_poigraphic',
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
            'overrideChildTca' => [
                'columns' => [
                    'uid_local' => [
                        'config' => [
                            'appearance' => [
                                'elementBrowserType' => 'file',
                                'elementBrowserAllowed' => 'svg',
                            ],
                        ],
                    ],
                ],
                'types' => [
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                    [
                        'showitem' => '--palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette, --palette--;;filePalette',
                    ],
                ],
            ],
            'filter' => [
                [
                    'userFunc' => 'TYPO3\\CMS\\Core\\Resource\\Filter\\FileExtensionFilter->filterInlineChildren',
                    'parameters' => [
                        'allowedFileExtensions' => 'svg',
                    ],
                ],
            ],
            'appearance' => [
                'useSortable' => false,
                'headerThumbnail' => [
                    'field' => 'uid_local',
                    'height' => '45m',
                ],
                'enabledControls' => [
                    'info' => true,
                    'new' => false,
                    'dragdrop' => true,
                    'sort' => false,
                    'hide' => true,
                    'delete' => true,
                ],
                'fileUploadAllowed' => false,
            ],
            'maxitems' => '1',
            'minitems' => '1',
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'SVG Graphic which contains POI-Objects with unique identifier (IDs)  ',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivepoi_poigraphic',
    ],
    'tx_hivepoi_poiremovesvgstyleattributes' => [
        'config' => [
            'type' => 'check',
            'renderType' => 'checkboxToggle',
            'default' => 0,
            'items' => [
                [
                    0 => '',
                    1 => '',
                ]
            ],
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ],
        'description' => 'remove Style Attributes from SVG to use own Styling',
        'exclude' => '1',
        'label' => 'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.tx_hivepoi_poiremovesvgstyleattributes',
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.CType.div._hivepoi_',
    '--div--',
];
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    'LLL:EXT:hive_poi/Resources/Private/Language/locallang_db.xlf:tt_content.CType.hivepoi_hive_poi',
    'hivepoi_hive_poi',
    'tx_hivepoi_hive_poi',
];
$tempTypes = [
    'hivepoi_hive_poi' => [
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => 1,
                ],
            ],
        ],
        'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,tx_hivepoi_poigraphic,tx_hivepoi_poicustomcssclasses,tx_hivepoi_poielement,tx_hivepoi_poiremovesvgstyleattributes,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
    ],
];
$GLOBALS['TCA']['tt_content']['types'] += $tempTypes;
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'hive_poi',
    'Configuration/TypoScript/',
    'hive_poi'
);

});

