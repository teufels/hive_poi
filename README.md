![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__swiperjs__simple-blue.svg)
![version](https://img.shields.io/badge/version-2.1.0-yellow.svg?style=flat-square)

HIVE Point of Interest (POI)
==========

Point of Interest Elements on one SVG-Graphic

#### This version supports TYPO3

![CUSTOMER](https://img.shields.io/badge/9_LTS-%23FFDB5A.svg?style=flat-square)
![SUPPORTS](https://img.shields.io/badge/10_LTS-%23A3C49B.svg?style=flat-square)
![SUPPORTS](https://img.shields.io/badge/11_LTS-%23A3C49B.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_poi`

## SVG Requirements
* objects to use must have a unique id and the class "poi"

## How to Use
1. Install Extenion `"beewilly/hive_poi": "*"` & Include in Root Template before hive_thm_custom
2. Get SVG Graphic with objects, which should be used, with a unique id and the class "poi"  
![INFO](https://img.shields.io/badge/info-SVG%20Object%20that%20are%20used%20poi%20element%20with%20corresponding%20id%20exist%20get%20the%20class%20inUse%20and%20become%20clickable-lightgrey?style=flat-square)  
![WARNING](https://img.shields.io/badge/warning-if%20used%20two%20or%20more%20POI--CE%20on%20same%20page%20be%20sure%20that%20SVG%20Object%20id%20is%20unique%20across%20all-red?style=flat-square)
3. Override Template 
4. Make your adjustments to Template  
![INFO](https://img.shields.io/badge/info-By%20default%20usage%20of%20Grid%20with%20Graphic%20right%20and%20InfoWindow%20left-lightgrey?style=flat-square)
5. Make your adjustments to Styling  
![INFO](https://img.shields.io/badge/info-Style%20stroke%2C%20fill%2C%20opacity%20for%20.poigraphic--container%20svg%20.poi%20general%2C%20%3Ahover%2C%20.active-lightgrey?style=flat-square)
6. Use Content Element HIVE / HIVE Point of Interest (POI)

***

### Code Examples

###### Override Template
```
plugin.tx_hivepoi {
    view {
        templateRootPaths >
        templateRootPaths {
            0 = EXT:hive_poi/Resources/Private/Templates/
            10 = EXT:hive_thm_custom/Resources/Private/Overrides/hive_poi/Templates/
        }
    }
}
```

***

## Changelog

- 1.0.7: add Translation for DE