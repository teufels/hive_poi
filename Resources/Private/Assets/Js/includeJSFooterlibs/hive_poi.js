/*
 *************************************************************************
 **                           LOCATIONS SCRIPT                          **
 *************************************************************************
*/

$(document).ready(function () {

    svgPOISetUsedItems();
    svgPOIClickHandler();
    labelPOIClickHandler();

    var onShowOnFirst = false;

    // On POI-Select change
    $('.tx-hive-poi select.poi--select').change( function () {
        var val = $(this).val();
        var parent = $(this).find(':selected').attr('data-parent');

        //console.log("onChange parent: " + parent + " val:" + val);

        if ( isNotNullOrEmpty(val) && isNotNullOrEmpty(parent) ) {
            console.log("do poiChange");
            poiChange(val, parent, onShowOnFirst);
        } else {
            $(this).val($.data(this, 'current'));
            return false;
        }

        $.data(this, 'current', $(this).val());
        onShowOnFirst = false;

    });

    // Select ShowOnFirst
    $(".tx-hive-poi select.poi--select option[data-showonfirst='1']").each(function() {

        var val = $(this).val();
        var parent = $(this).attr('data-parent');

        //console.log("onFirst parent: " + parent + " val:" + val);

        onShowOnFirst = true;
        $('.tx-hive-poi #poi-' + parent + '-select').val(val).change();

    });

});//end Document ready



/*
 * add class for in use svg items
 */
function svgPOISetUsedItems() {

    $('.tx-hive-poi select.poi--select option').each(function() {
        var $svgObjectId = $(this).val();
        var $svgObjectLabel = $(this).text();

        if( $svgObjectId != ""){
            $('.poigraphic-container svg #' + $svgObjectId).addClass("inUse");

            if( $svgObjectLabel != ""){
                $('.poigraphic-container svg #' + $svgObjectId).attr('title', $svgObjectLabel);
            }
        }

    });
}


/*
 * set active on svg click
 */
function svgPOIClickHandler() {
    $('.tx-hive-poi .poigraphic-container svg .poi').unbind('click').bind('click', function (e) {
        var $active_poielementsvgid = $(this).attr("id");
        //set POI Select Value
        $('.tx-hive-poi select.poi--select').val($active_poielementsvgid).change();
    });
}

/*
* set active on label click
*/
function labelPOIClickHandler() {
    $('.tx-hive-poi .poi--label').unbind('click').bind('click', function (e) {
        var $active_poielementsvgid = $(this).attr("data-poielement-svgid");
        //set POI Select Value
        $('.tx-hive-poi select.poi--select').val($active_poielementsvgid).change();
    });
}

/*
 * POI (Select) change
 */
function poiChange( $svgid, $parent, $onShowOnFirst ) {


    if ($svgid != "") {

        //POI--list
        $('.poi--element[data-parent="' + $parent + '"]').fadeOut(400);
        $('.poi--element[data-parent="' + $parent + '"]').removeClass("active");

        $('*[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').delay(400).fadeIn(400);
        $('*[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').addClass("active");

        //svg-graphic
        $('#poi-' + $parent + '-graphic svg .poi').removeClass("active");
        $('#poi-' + $parent + '-graphic svg #' + $svgid).addClass("active");

        //check if linked
        var link = $('.poi--element[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').attr("data-poielement-link");
        var uri = $('.poi--element[data-poielement-svgid="' + $svgid + '"][data-parent="' + $parent + '"]').attr("data-poielement-link-uri");
        if(link != "" && uri != "") {
            var target = link.split(' ')[1];
            if(typeof target == 'undefined' || target == 'undefined' || target == '') {
                target = "_self";
            }

            //console.log("uri: " + uri + ", target: " + target);
            window.open(uri, target);
        }

    } else {

        //POI--list
        $('.poi--element[data-parent="' + $parent + '"]').fadeOut(400);
        $('.poi--element[data-parent="' + $parent + '"]').removeClass("active");

        //svg-graphic
        $('#poi-' + $parent + '-graphic svg .poi').removeClass("active");

    }

    // scroll to results
    if(!$onShowOnFirst){

        var scrollTarget = "";

        if(link != "" && uri != "") {
            scrollTarget = $("a[name=" +  uri.substr(uri.indexOf('#')+1) + "]");
        } else {
            scrollTarget = $("a[name=poi-" + $parent + "-results]");
            if(scrollTarget.length && scrollTarget != "" && scrollTarget != "undefined"){
                console.log("poi scollTo: " + scrollTarget);
                $('html, body').animate({scrollTop: scrollTarget.offset().top + 'px'}, 600);
            }
        }
    }

}

function isNotNullOrEmpty (val) {
    switch (val) {
        case "":
            return false;
        case 0:
            return false;
        case null:
            return false;
        case false:
            return false;
        case undefined:
            return false;
        case typeof this === 'undefined':
            return false;
        default: return true;
    }
}