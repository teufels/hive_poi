<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "hive_poi".
 *
 * Auto generated 19-10-2020 09:18
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'HIVE>Point of Interest',
  'description' => 'Point of Interest Elements on SVG-Graphic',
  'category' => 'fe',
  'author' => 'Albert Giss, Alexander Neher, Andreas Hafner, Bastian Holzem, Hendrik Krueger, Karim Hamid, Marcel Weber, Timo Bittner, Yannick Aister, Zoe Kern',
  'author_email' => 'a.giss@teufels.com, a.neher@teufels.com, a.hafner@teufels.com, b.holzem@teufels.com, h.krueger@teufels.com, k.hamid@teufels.com, m.weber@teudels.com, t.bittner@teufels.com, y.aister@teufels, z.kern@teufels.com',
  'state' => 'stable',
  'version' => '2.2.1',
  'constraints' =>
  array (
    'depends' =>
    array (
      'typo3' => '9.5.0-11.5.99',
    ),
    'conflicts' =>
    array (
    ),
    'suggests' =>
    array (
    ),
  ),
);

