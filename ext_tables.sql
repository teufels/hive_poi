CREATE TABLE tt_content (
    tx_hivepoi_poielementcontent_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_hivepoi_poielementcontent_parent (tx_hivepoi_poielementcontent_parent,pid,deleted),
    tx_hivepoi_poicustomcssclasses tinytext,
    tx_hivepoi_poielement int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivepoi_poigraphic int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivepoi_poiremovesvgstyleattributes tinyint(4) DEFAULT '0' NOT NULL,
);
CREATE TABLE tx_hivepoi_poielement (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_id int(11) DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    tx_hivepoi_poielcustomcssclasses tinytext,
    tx_hivepoi_poielementcontent int(11) unsigned DEFAULT '0' NOT NULL,
    tx_hivepoi_poielementlabel tinytext,
    tx_hivepoi_poielementlink tinytext,
    tx_hivepoi_poielementsvgid tinytext,
    tx_hivepoi_poielementshowonfirst tinyint(4) DEFAULT '0' NOT NULL,
    KEY language (l10n_parent,sys_language_uid)
);
